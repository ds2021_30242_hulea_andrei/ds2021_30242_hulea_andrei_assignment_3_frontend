import rpc from "./rpc";

export function getHourly(body) {
    return rpc.request("dDaysHourDetails", body)
}

export function getBaseline(body){
    return rpc.request("baseline", body)
}

export function getProgram(body){
    return rpc.request("getProgram", body)
}