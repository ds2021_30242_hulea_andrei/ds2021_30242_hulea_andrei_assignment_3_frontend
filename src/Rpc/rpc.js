import { JSONRPCClient } from "json-rpc-2.0";

const rpc = new JSONRPCClient((jsonRPCRequest) =>
    fetch("https://online-energy-utility-platform.herokuapp.com/rpc", {
    //fetch("http://localhost:8080/rpc", {
        method: "POST",
        headers: {
            "content-type": "application/json",
        },
        body: JSON.stringify(jsonRPCRequest),
    }).then((response) => {
        console.log(JSON.stringify(jsonRPCRequest))
        if (response.status === 200) {
            return response
                .json()
                .then((jsonRPCResponse) => rpc.receive(jsonRPCResponse));
        } else if (jsonRPCRequest.id !== undefined) {
            return Promise.reject(new Error(response.statusText));
        }
    })
);

export default rpc;

