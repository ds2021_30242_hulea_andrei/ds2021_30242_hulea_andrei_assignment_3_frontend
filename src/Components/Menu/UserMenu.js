import Navbar from "react-bootstrap/Navbar"
import {Nav} from "react-bootstrap"
import React from "react";
import './Menu.css';

const UserMenu = () => {

    return(

        <div>
        <ul>
            <li style={{fontSize:'20px',marginRight:'10px'}}><a href="/user"> Energy platform </a></li>

            <li style={{marginTop:'3px'}}><a href="/user"> Devices and sensors </a></li>
            <li style={{marginTop:'3px'}}><a href="/user/history"> Energy consumption </a></li>
            <li style={{marginTop:'3px'}}><a href="/user/graph"> Energy graph </a></li>
            <li style={{marginTop:'3px'}}><a href="/user/detailedgraph"> Detailed graph </a></li>
            <li style={{marginTop:'3px'}}><a href="/user/baseline"> Baseline graph </a></li>
            <li style={{marginTop:'3px'}}><a href="/user/program"> Program </a></li>


            <li style={{float:'right',marginTop:'3px'}}><a href="/login"> Log out </a></li>
        </ul>

        </div>
    )

}
export default UserMenu;