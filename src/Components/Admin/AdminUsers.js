import React from "react";
import Menu from "../Menu/Menu";
import {DataGrid} from "@mui/x-data-grid";
import axiosInstance from "../../axios";
import {Button, Container, TextField} from "@material-ui/core";
import './AdminUsers.scss';

const userColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'name', headerName: 'Name', width: 200},
    {field: 'age', headerName: 'Age', width: 125},
    {field: 'address', headerName: 'Address', width: 200},
    {field: 'username', headerName: 'Username', width: 150},
    {field: 'password', headerName: 'Password', width: 150},
    {field: 'role', headerName: 'Role', width: 150},
    {field: 'deviceList', headerName: 'Device List', width: 300}
]

const deviceColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 200},
    {field: 'location', headerName: 'Location', width: 200},
    {field: 'maxEnergyConsumption', headerName: 'Max', width: 150},
    {field: 'averageEnergyConsumption', headerName: 'Avg', width: 150},
    {field: 'sensor', headerName: 'Sensor', width: 200},
]

export default class AdminUsers extends React.Component {


    constructor() {
        super();

        this.state = {
            users: [],
            devices: [],
            id:"",
            name:"",
            age:0,
            address:"",
            username:"",
            password:"",
            role:"",
            deviceList:"",
            device_id:""
        }

        this.addUser = this.addUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.addUserDevice = this.addUserDevice.bind(this);
        this.removeUserDevice = this.removeUserDevice.bind(this);

        axiosInstance
            .get("/user",
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response => {

                this.setState({users: response.data});

                for(let i=0;i<response.data.length;i++){

                    let deviceList_aux = ""

                    for(let j=0;j< response.data[i].deviceList.length;j++)
                        deviceList_aux += response.data[i].deviceList[j].id + " "

                    this.state.users[i].deviceList = deviceList_aux;
                }

            })
            .catch(error => {
                console.log(error);
            })

        axiosInstance
            .get("/device",
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response => {
                this.setState({devices: response.data});

                for(var i=0;i<response.data.length;i++){
                    this.state.devices[i].sensor = response.data[i]?.sensor?.id;
                    console.log(response.data[i]?.sensor?.id)
                }
            })
            .catch(error => {
                console.log(error);
            })
    }


    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
    };


    addUser(){

        let newUser = {
            name:this.state.name,
            age:this.state.age,
            address:this.state.address,
            username:this.state.username,
            password:this.state.password,
            role:this.state.role,
            deviceList:this.state.deviceList
        }

        axiosInstance.post("/user",newUser,
            {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
        .then(console.log("ok"))
        .catch(error => {
            console.log(error)
        })

    }

    addUserDevice(){

        let userId = this.state.id;
        let deviceId = this.state.device_id;

        console.log("user id: " +  userId);
        console.log("device id:" + deviceId);

        axiosInstance
            .put("/user/" + userId + "/" + deviceId,{},
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })
    }

    removeUserDevice(){

        let userId = this.state.id;
        let deviceId = this.state.device_id;

        axiosInstance
            .put("/user/remove/" + userId + "/" + deviceId,{},
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })

    }

    editUser(){


        let newUser = {
            id:this.state.id,
            name:this.state.name,
            age:this.state.age,
            address:this.state.address,
            username:this.state.username,
            password:this.state.password,
            role:this.state.role
        }

        axiosInstance
            .put("/user/" + newUser.id,newUser,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })




    }

    deleteUser(){

        console.log(this.state.id)

        axiosInstance
            .delete("/user/" + this.state.id,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok del"))
            .catch(error =>{
                console.log(error)
            })


    }


    render() {
        return (


            <div>

                <Menu/>


                <div style={{display: 'flex', justifyContent: 'center',
                    alignItems: 'center', 'marginTop': '100px'}}>
                    <div style={{display: 'flex', height: 500, width: 1750}}>
                        <div style={{flexGrow: 5}}>
                            <DataGrid on rows={this.state.users} columns={userColumns} pageSize={6} autoHeight="true"
                            />
                        </div>
                    </div>
                </div>





                <div className="split">

                    <Container style={{width:'25%'}}>

                        <h4>User</h4>

                        <div>
                            <TextField
                                style={{'marginRight': '30px'}}
                                label="User Id"
                                name="id"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                label="Name"
                                name="name"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div>

                            <TextField
                                style={{'marginRight': '30px', 'marginBottom': '10px'}}
                                label="Age"
                                name="age"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                style={{'marginBottom': '10px'}}
                                label="Address"
                                name="address"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div>
                            <TextField
                                style={{'marginRight': '30px', 'marginBottom': '10px'}}
                                label="Username"
                                name="username"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                style={{'marginBottom': '10px'}}
                                label="Password"
                                name="password"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div>
                            <TextField
                                style={{'marginRight': '30px', 'marginBottom': '10px'}}
                                label="Role"
                                name="role"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                style={{'marginBottom': '10px'}}
                                label="Device Id"
                                name="device_id"
                                onChange = {this.handleInput}

                            />
                        </div>


                        <div>
                            <Button
                                style={{marginLeft:'15px',marginRight:'10px',marginTop:'10px'}}
                                onClick={this.addUser}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Add
                            </Button>
                            <Button
                                style={{marginLeft:'10px',marginRight:'10px',marginTop:'10px'}}
                                onClick={this.editUser}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Edit
                            </Button>
                            <Button
                                style={{marginLeft:'10px',marginRight:'10px',marginTop:'10px'}}
                                onClick={this.deleteUser}
                                type = "submit"
                                variant="contained"
                                color="secondary">
                                Delete
                            </Button>
                            <Button
                                style={{marginLeft:'10px',marginRight:'10px',marginTop:'10px'}}
                                onClick={this.addUserDevice}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Add device
                            </Button>

                            <Button
                                style={{marginLeft:'10px',marginRight:'10px',marginTop:'10px'}}
                                onClick={this.removeUserDevice}
                                type = "submit"
                                variant="contained"
                                color="secondary">
                                Remove device
                            </Button>
                        </div>

                    </Container>


                    <div style={{width:'55%',display: 'flex',
                        justifyContent: 'center', alignItems: 'center',
                        marginRight:'75px'}}>
                        <div style={{display: 'flex', height: 350, width: 1750}}>
                            <div style={{flexGrow: 5}}>
                                <DataGrid on rows={this.state.devices} columns={deviceColumns} pageSize={4}
                                          autoHeight="true"
                                />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

}