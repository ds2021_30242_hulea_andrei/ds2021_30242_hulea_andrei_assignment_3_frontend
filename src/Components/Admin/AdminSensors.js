import React from 'react';
import Menu from '../Menu/Menu'
import axiosInstance from "../../axios";
import {DataGrid} from "@mui/x-data-grid";
import {Button, Container, TextField} from "@material-ui/core";


const sensorColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 300},
    {field: 'max_value', headerName: 'Max', width: 150},
    {field: 'monitoredValueList', headerName: 'Monitored Values', width: 400}
]
export default class AdminSensors extends React.Component{

    constructor() {
        super();

        this.state={
            sensors:[],
            id:0,
            description:"",
            max_value:0,
            monitoredValueList:""
        }

        this.addSensor = this.addSensor.bind(this)
        this.editSensor = this.editSensor.bind(this)
        this.deleteSensor = this.deleteSensor.bind(this)

        axiosInstance
            .get("/sensor",
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response =>{
                this.setState({sensors:response.data})

                for(let i=0;i<this.state.sensors.length;i++){
                    let monitoredList_aux = ""

                    for(let j=0;j<this.state.sensors[i].monitoredValueList.length;j++)
                        monitoredList_aux += this.state.sensors[i].monitoredValueList[j].id.toString() + " "

                    this.state.sensors[i].monitoredValueList = monitoredList_aux;
                }
            })
            .catch(error => {
                console.log(error);
            })

    }

    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
    };

    addSensor(){
        let newSensor = {
            description: this.state.description,
            max_value: this.state.max_value
        }

        console.log(this.state.max_value);

        axiosInstance
            .post("/sensor",newSensor,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok"))
            .catch(error => {
                console.log(error)
            })
    }

    editSensor(){
        let newSensor = {
            id:this.state.id,
            description: this.state.description,
            max_value: this.state.max_value
        }

        axiosInstance
            .put("/sensor/" + newSensor.id, newSensor,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })
    }

    deleteSensor(){

        axiosInstance
            .delete("/sensor/" + this.state.id,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok del"))
            .catch(error =>{
                console.log(error)
            })

    }



    render(){
        return(
            <div>


                <Menu/>

                <div style={{marginTop:'100px',display: 'flex',
                    justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{display: 'flex', height: 350, width: 1750}}>
                        <div style={{flexGrow: 5}}>
                            <DataGrid on rows={this.state.sensors} columns={sensorColumns} pageSize={6}
                                      autoHeight="true"
                            />
                        </div>
                    </div>
                </div>


                <div>
                <Container
                    style={{marginTop:'100px',marginLeft:'650px'}}>

                    <h4>Sensor</h4>

                    <div>
                        <TextField
                            style={{'marginRight': '30px'}}
                            label="Sensor Id"
                            name="id"
                            onChange = {this.handleInput}

                        />

                        <TextField
                            label="Description"
                            name="description"
                            onChange = {this.handleInput}

                        />
                    </div>

                    <div>
                        <TextField
                            style={{'marginRight': '30px', 'marginBottom': '10px'}}
                            label="Max"
                            name="max_value"
                            onChange = {this.handleInput}

                        />

                        <TextField
                            style={{'marginBottom': '10px'}}
                            label="Monitored Values"
                            name="monitoredValueList"
                            onChange = {this.handleInput}

                        />
                    </div>


                    <div style={{marginTop:'50px',marginLeft:'70px'}}>
                        <Button
                            style={{'marginLeft':'10px','marginRight':'10px'}}
                            onClick={this.addSensor}
                            type = "submit"
                            variant="contained"
                            color="primary">
                            Add
                        </Button>
                        <Button
                            style={{'marginLeft':'10px','marginRight':'10px'}}
                            onClick={this.editSensor}
                            type = "submit"
                            variant="contained"
                            color="primary">
                            Edit
                        </Button>
                        <Button
                            style={{'marginLeft':'10px','marginRight':'10px'}}
                            onClick={this.deleteSensor}
                            type = "submit"
                            variant="contained"
                            color="secondary">
                            Delete
                        </Button>


                    </div>
                </Container>
                </div>

            </div>
        )
    }
}