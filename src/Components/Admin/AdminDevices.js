import React from 'react';
import Menu from '../Menu/Menu';
import {DataGrid} from "@mui/x-data-grid";
import axiosInstance from "../../axios";
import {Button, Container, TextField} from "@material-ui/core";


const deviceColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 300},
    {field: 'location', headerName: 'Location', width: 200},
    {field: 'maxEnergyConsumption', headerName: 'Max', width: 150},
    {field: 'averageEnergyConsumption', headerName: 'Avg', width: 150},
    {field: 'sensor', headerName: 'Sensor', width: 300},
]

const sensorColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 300},
    {field: 'max_value', headerName: 'Max', width: 150},
    {field: 'monitoredValueList', headerName: 'Monitored Values', width: 200}
]

export default class AdminDevices extends React.Component{

    constructor() {
        super()

        this.state={
            devices:[],
            sensors:[],
            id:0,
            description:"",
            location:"",
            maxEnergyConsumption:0,
            averageEnergyConsumption:0,
            sensor:0
        }

        this.addDevice = this.addDevice.bind(this)
        this.addDeviceSensor = this.addDeviceSensor.bind(this)
        this.removeDeviceSensor = this.removeDeviceSensor.bind(this)
        this.deleteDevice = this.deleteDevice.bind(this)
        this.updateDevice = this.updateDevice.bind(this)


        axiosInstance
            .get("/device",
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response => {

                console.log(response.data)
                this.setState({devices: response.data});

                //console.log("senssssor",response.data[12].sensor.id)
                for(var i=0;i<response.data.length;i++){
                    this.state.devices[i].sensor = response.data[i]?.sensor?.id;
                    console.log(response.data[i]?.sensor?.id)
                }
                console.log("asdsadsad",this.state.devices)

            })
            .catch(error => {
                console.log(error);
            })

        axiosInstance
            .get("/sensor",
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response =>{
                this.setState({sensors:response.data})

                for(let i=0;i<this.state.sensors.length;i++){
                    let monitoredList_aux = ""

                    for(let j=0;j<this.state.sensors[i].monitoredValueList.length;j++)
                        monitoredList_aux += this.state.sensors[i].monitoredValueList[j].id.toString() + " "

                    this.state.sensors[i].monitoredValueList = monitoredList_aux;
                }
            })
            .catch(error => {
                console.log(error);
            })
    }



    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
    };


    addDevice(){

        let newDevice = {
            description: this.state.description,
            location: this.state.location,
            averageEnergyConsumption: this.state.averageEnergyConsumption,
            maxEnergyConsumption: this.state.maxEnergyConsumption,
            sensor: this.state.sensor
        }

        axiosInstance
            .post("/device",newDevice,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok"))
            .catch(error => {
                console.log(error)
            })
    }

    addDeviceSensor(){

        let deviceId = this.state.id
        let sensorId = this.state.sensor

        console.log("deviceId:" +  deviceId);
        console.log("sensorId:" + sensorId);

        axiosInstance
            .put("/device/" + deviceId + "/" + sensorId,{},
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })

    }

    removeDeviceSensor(){

        let deviceId = this.state.id
        let sensorId = this.state.sensor

        axiosInstance
            .put("/device/remove/" + deviceId + "/" + sensorId,{},
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })

    }

    updateDevice(){
        let newDevice = {
            id:this.state.id,
            description: this.state.description,
            location: this.state.location,
            averageEnergyConsumption: this.state.averageEnergyConsumption,
            maxEnergyConsumption: this.state.maxEnergyConsumption,
        }

        axiosInstance
            .put("/device/" + newDevice.id,newDevice,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok update"))
            .catch(error => {
                console.log(error)
            })
    }

    deleteDevice(){

        console.log(this.state.id)

        axiosInstance
            .delete("/device/" + this.state.id,
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(console.log("ok del"))
            .catch(error =>{
                console.log(error)
            })

    }


    render(){
        return(
            <div>
                <Menu/>

                <div style={{marginTop:'100px',display: 'flex',
                    justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{display: 'flex', height: 350, width: 1750}}>
                        <div style={{flexGrow: 5}}>
                            <DataGrid on rows={this.state.devices}
                                      columns={deviceColumns}
                                      pageSize={4}
                                      autoHeight="true"
                            />
                        </div>
                    </div>
                </div>


                <div className="split">
                    <Container
                        style={{width:'35%',marginLeft:'300px'}}>

                        <h4>Device</h4>

                        <div>
                            <TextField
                                style={{'marginRight': '30px'}}
                                label="Device Id"
                                name="id"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                label="Description"
                                name="description"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div>
                            <TextField
                                style={{'marginRight': '30px', 'marginBottom': '10px'}}
                                label="Location"
                                name="location"
                                onChange = {this.handleInput}

                            />

                            <TextField
                                style={{'marginBottom': '10px'}}
                                label="Max"
                                name="maxEnergyConsumption"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div>
                            <TextField
                                style={{'marginRight': '30px', 'marginBottom': '10px'}}
                                label="Avg"
                                name="averageEnergyConsumption"
                                onChange = {this.handleInput}
                            />

                            <TextField
                                style={{'marginBottom': '10px'}}
                                label="Sensor id"
                                name="sensor"
                                onChange = {this.handleInput}

                            />
                        </div>

                        <div style={{'marginTop':'50px'}}>
                            <Button
                                style={{'marginLeft':'10px','marginRight':'10px'}}
                                onClick={this.addDevice}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Add
                            </Button>
                            <Button
                                style={{'marginLeft':'10px','marginRight':'10px'}}
                                onClick={this.updateDevice}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Edit
                            </Button>
                            <Button
                                style={{'marginLeft':'10px','marginRight':'10px'}}
                                onClick={this.deleteDevice}
                                type = "submit"
                                variant="contained"
                                color="secondary">
                                Delete
                            </Button>
                        </div>
                        <div style={{marginTop:'10px'}}>
                            <Button
                                style={{marginLeft:'10px'}}
                                onClick={this.addDeviceSensor}
                                type = "submit"
                                variant="contained"
                                color="primary">
                                Add sensor
                            </Button>

                            <Button
                                onClick={this.removeDeviceSensor}
                                style={{marginLeft:'10px'}}
                                type = "submit"
                                variant="contained"
                                color="secondary">
                                Remove sensor
                            </Button>

                        </div>
                    </Container>

                    <div style={{marginRight:'280px',width:'40%',
                        marginTop:'25px',display: 'flex',
                        justifyContent: 'center', alignItems: 'center'}}>
                        <div style={{display: 'flex', height: 350, width: 1750}}>
                            <div style={{flexGrow: 5}}>
                                <DataGrid on rows={this.state.sensors} columns={sensorColumns} pageSize={3}
                                          autoHeight="true"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
