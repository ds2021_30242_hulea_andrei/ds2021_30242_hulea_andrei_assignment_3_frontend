import React from 'react';
import UserMenu from '../Menu/UserMenu';
import axiosInstance from "../../axios";
import {DataGrid} from "@mui/x-data-grid";
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";

const deviceColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 300},
    {field: 'location', headerName: 'Location', width: 200},
    {field: 'maxEnergyConsumption', headerName: 'Max', width: 150},
    {field: 'averageEnergyConsumption', headerName: 'Avg', width: 150},
    {field: 'sensor', headerName: 'Sensor Id', width: 300},
]


const sensorColumns = [
    {field: 'id', headerName: 'Id', width: 100},
    {field: 'description', headerName: 'Description', width: 300},
    {field: 'max_value', headerName: 'Max', width: 150},
    {field: 'monitoredValueList', headerName: 'Monitored Values', width: 1100}
]
export default class UserDevices extends React.Component{


    componentDidMount() {
        this.connect();
    }

    connect(){
        const websocket = new SockJS("https://online-energy-utility-platform.herokuapp.com/socket")
        //const websocket = new SockJS("http://localhost:8080/socket")
        const stompClient = Stomp.over(websocket);

        stompClient.connect({}, frame =>{
            console.log(frame)
            stompClient.subscribe("/topic/socket/notif/" + localStorage.getItem("USER_ID"), notification => {
                alert(notification.body)
            })
        })
    }



    constructor() {
        super();
        this.state={
            devices:[],
            sensors:[]
        }

        axiosInstance
            .get("/user/devices/" + localStorage.getItem("USER_ID"),
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then( response => {

                this.setState({devices: response.data.filter(x => x !==null)})

                for(let i=0;i<response.data.length;i++) {
                    this.state.devices[i].sensor = response.data[i]?.sensor?.id;
                }
                })
            .catch(error => {
                console.log(error);
            })

        axiosInstance
            .get("/user/devices/sensors/" + localStorage.getItem("USER_ID"),
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(  response =>{

                this.setState({sensors:response.data})

                for(let i=0;i<this.state.sensors.length;i++){
                    let monitoredList_aux = ""

                    for(let j=0;j<this.state.sensors[i].monitoredValueList.length;j++)
                        monitoredList_aux += this.state.sensors[i].monitoredValueList[j].id.toString() + " "

                    this.state.sensors[i].monitoredValueList = monitoredList_aux;
                }
                }
            )
            .catch(error => {
            console.log(error);
        })



    }





    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
    };

    render() {
        return (
            <div>
                <UserMenu/>

                <h3 style={{marginTop:'100px',marginLeft:'100px'}}>Device list</h3>
                <div style={{marginTop:'20px',display: 'flex',
                    justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{display: 'flex', height: 350, width: 1750}}>
                        <div style={{flexGrow: 5}}>
                            <DataGrid on rows={this.state.devices}
                                      columns={deviceColumns}
                                      pageSize={4}
                                      autoHeight="true"
                            />
                        </div>
                    </div>
                </div>

                <h3 style={{marginTop:'30px',marginLeft:'100px'}}>Sensor list</h3>

                <div style={{marginTop:'20px',display: 'flex',
                    justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{display: 'flex', height: 350, width: 1750}}>
                        <div style={{flexGrow: 5}}>
                            <DataGrid on rows={this.state.sensors}
                                      columns={sensorColumns}
                                      pageSize={4}
                                      autoHeight="true"
                            />
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
