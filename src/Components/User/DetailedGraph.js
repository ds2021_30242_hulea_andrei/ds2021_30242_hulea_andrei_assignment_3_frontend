import React from "react";
import UserMenu from "../Menu/UserMenu";
import {getHourly} from '../../Rpc/rpcCall'
import {Button, TextField} from "@material-ui/core";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import './DetailedGraph.css'


export default class UserGraph extends React.Component{

    constructor(props) {
        super(props);
        this.state ={

            graphDetails : [],
            device : 0,
            days : 0,
            days_length : 0,
            days_vec : [],
            series_vec : [{
                name: '00:00',
                data: []
            }, {
                name: '01:00',
                data: []
            }, {
                name: '02:00',
                data: []
            }, {
                name: '03:00',
                data: []
            }, {
                name: '04:00',
                data: []
            }, {
                name: '05:00',
                data: []
            }, {
                name: '06:00',
                data: []
            }, {
                name: '07:00',
                data: []
            }, {
                name: '08:00',
                data: []
            }, {
                name: '09:00',
                data: []
            }, {
                name: '10:00',
                data: []
            }, {
                name: '11:00',
                data: []
            }, {
                name: '12:00',
                data: []
            }, {
                name: '13:00',
                data: []
            }, {
                name: '14:00',
                data: []
            }, {
                name: '15:00',
                data: []
            }, {
                name: '16:00',
                data: []
            }, {
                name: '17:00',
                data: []
            }, {
                name: '18:00',
                data: []
            }, {
                name: '19:00',
                data: []
            }, {
                name: '20:00',
                data: []
            }, {
                name: '21:00',
                data: []
            }, {
                name: '22:00',
                data: []
            }, {
                name: '23:00',
                data: []
            }]
        }

       this.getGraphDetails = this.getGraphDetails.bind(this)

    }


    getGraphDetails(){

        let body = {
            userId:localStorage.getItem("USER_ID"),
            deviceId:this.state.device,
            days:this.state.days
        }

        getHourly(body)
            .then(
                response =>{
                    //console.log("RESPONSE",response)


                    this.setState({graphDetails:response})
                    this.setState({days_length:response.length})



                    console.log(this.state.graphDetails)

                    let days_vec_aux = []

                    for(let i=0; i<response.length; i++){
                        days_vec_aux[i] = response.length - i;

                        for(let j = 0; j<response[i].length;j++)
                            this.state.series_vec[j].data[i] = response[i][j]

                    }

                    this.setState({days_vec : days_vec_aux})

                }
            )
            .catch(error => {
                console.log(error)
            })


    }


    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };


    render(){



        const configObj = {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Hourly energy consumption'
            },
            xAxis: {
                categories: this.state.days_vec
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Energy consumption'
                }
            },
            legend: {
                reversed: false
            },
            plotOptions: {
                bar: {
                    showInLegend: true,
                },
                series: {
                    stacking: 'normal'
                }
            },
            series: this.state.series_vec
        };


        return(
            <div>

                <UserMenu/>

                <div>

                    <TextField
                        style={{
                            marginTop:'30px',
                            marginLeft:'20px'
                        }}
                        name="device"
                        placeholder="Device Id"
                        onChange={this.handleInput}
                    />

                    <TextField
                        style={{
                            marginTop:'30px',
                            marginLeft:'20px'
                        }}
                        name="days"
                        placeholder="Nr. of days"
                        onChange={this.handleInput}
                    />

                    <Button
                        style={{
                            marginTop:'30px',
                            marginLeft:'20px'
                        }}
                        onClick={this.getGraphDetails}
                        type = "submit"
                        variant="contained"
                        color="primary"
                    >
                       Search
                    </Button>



                </div>

                <HighchartsReact
                    highcharts={Highcharts}
                    options={configObj}
                />


            </div>
        )

    }
}