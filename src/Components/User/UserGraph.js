import React from 'react';
import axiosInstance from "../../axios";
import {Button, TextField} from "@material-ui/core";
import UserMenu from "../Menu/UserMenu";
import {Bar} from 'react-chartjs-2'
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";




export default class UserGraph extends React.Component{


    componentDidMount() {
        this.connect();
    }

    connect(){
        const websocket = new SockJS("https://online-energy-utility-platform.herokuapp.com/socket")
        //const websocket = new SockJS("http://localhost:8080/socket")
        const stompClient = Stomp.over(websocket);

        stompClient.connect({}, frame =>{
            console.log(frame)
            stompClient.subscribe("/topic/socket/notif/" + localStorage.getItem("USER_ID"), notification => {
                alert(notification.body)
            })
        })
    }


    constructor(props) {
        super(props);
        this.state = {
            monitoredValues:[],
            monitoredValuesList:[],
            date:"",
            hourlyValues:[]

        }

        this.getGraph = this.getGraph.bind(this)

        axiosInstance
            .get("/user/devices/sensors/monitored/" + localStorage.getItem("USER_ID"),
                {headers: { 'Authorization' : 'Bearer ' + localStorage.getItem("TOKEN")}})
            .then(response => {
                console.log(response.data)
                this.state.monitoredValues = response.data

                console.log(this.state.monitoredValues[0].id)

            })
            .catch(error => {
                console.log(error);
            })
    }


    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    getGraph(){

        console.log(this.state.monitoredValues[0].timestamp.substring(11,13))
        console.log(this.state.monitoredValues[0].value)
        // let monitoredDate = this.state.monitoredValues[0].timestamp.substring(0,10)

        // console.log("monitored",monitoredDate)
        console.log(this.state.date)
        //
        // if(monitoredDate === this.state.date)
        //     console.log("ZZZZZZ")

        let ok = false
        let monitoredData = ""
        let indexes = []
        console.log(this.state.monitoredValues.length)

        for(let i=0; i<this.state.monitoredValues.length;i++)
        {
            let monitoredDate_aux = this.state.monitoredValues[i].timestamp.substring(0,10)

            if(this.state.date === monitoredDate_aux)
            {
                // eslint-disable-next-line no-unused-vars
                monitoredData = monitoredDate_aux
                ok = true
                indexes.push(i)
            }
        }


        let hourlySums = []
        for(let i=0;i<=23;i++)
            hourlySums[i] = 0

        if(ok===true){

                indexes.forEach(

                    i => {

                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "00"){
                            hourlySums[0] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "01"){
                            hourlySums[1] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "02"){
                            hourlySums[2] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "03"){
                            hourlySums[3] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "04"){
                            hourlySums[4] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "05"){
                            hourlySums[5] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "06"){
                            hourlySums[6] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "07"){
                            hourlySums[7] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "08"){
                            hourlySums[8] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "09"){
                            hourlySums[9] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "10"){
                            hourlySums[10] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "11"){
                            hourlySums[11] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "12"){
                            hourlySums[12] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "13"){
                            hourlySums[13] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "14"){
                            hourlySums[14] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "15"){
                            hourlySums[15] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "16"){
                            hourlySums[16] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "17"){
                            hourlySums[17] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "18"){
                            hourlySums[18] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "19"){
                            hourlySums[19] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "20"){
                            hourlySums[20] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "21"){
                            hourlySums[21] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "22"){
                            hourlySums[22] += this.state.monitoredValues[i].value
                        }
                        if(this.state.monitoredValues[i].timestamp.substring(11,13) === "23"){
                            hourlySums[23] += this.state.monitoredValues[i].value
                        }
                    }
                )

            console.log("sume",hourlySums)
            this.setState({hourlyValues:hourlySums})
        }
    }

    render(){
        return(
            <div>
                <UserMenu/>

                <TextField
                    style={{
                        marginTop:'100px',
                        marginLeft:'70px'
                    }}
                    type="date"
                    name="date"
                    onChange = {this.handleInput}
                />

                <Button
                    type = "submit"
                    variant="contained"
                    color="primary"
                    style={{marginTop:'100px',marginLeft:'20px'}}
                    onClick={this.getGraph}
                >
                    View
                </Button>

                <div>
                    <Bar
                        data={{
                            labels: ['00:00',
                                    '01:00',
                                    '02:00',
                                    '03:00',
                                    '04:00',
                                    '05:00',
                                    '06:00',
                                    '07:00',
                                    '08:00',
                                    '09:00',
                                    '10:00',
                                    '11:00',
                                    '12:00',
                                    '13:00',
                                    '14:00',
                                    '15:00',
                                    '16:00',
                                    '17:00',
                                    '18:00',
                                    '19:00',
                                    '20:00',
                                    '21:00',
                                    '22:00',
                                    '23:00',
                            ],
                            datasets: [
                                {
                                    label: 'Energy Consumption',
                                    data: this.state.hourlyValues,
                                    backgroundColor: [
                                        'rgba(0,111,255,0.3)'
                                    ],
                                    borderColor: [
                                        'rgb(255,255,255)'
                                    ],
                                    borderWidth: 1,
                                }
                            ],
                        }}
                        height={700}
                        width={700}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                            legend: {
                                labels: {
                                    fontSize: 25,
                                },
                            },
                        }}
                    />
                </div>

            </div>
        )
    }

}