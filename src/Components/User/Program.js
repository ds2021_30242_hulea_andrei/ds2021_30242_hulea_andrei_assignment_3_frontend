import React from 'react'
import UserMenu from "../Menu/UserMenu";
import {Button, TextField} from "@material-ui/core";
import {getBaseline, getProgram} from "../../Rpc/rpcCall";
import {Bar} from "react-chartjs-2";

export default class Program extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            device:0,
            hours:0,
            graphValues:[],
            baseline:[]

        }
        this.getProgram = this.getProgram.bind(this)


    }


    getProgram(){

        let body = {
            userId:localStorage.getItem("USER_ID"),
            deviceId:this.state.device,
            hours:this.state.hours
        }


        getProgram(body)
            .then(
                response => {
                    console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
                    this.setState({graphValues:response})
                    console.log("PROGRAM",this.state.graphValues)
                    console.log(Object.keys(this.state.graphValues).length)


                    /*if(Object.keys(this.state.graphValues).length === 0)
                        alert("Choose a smaller interval!")*/
                }
            )


        let body2 = {
            userId:localStorage.getItem("USER_ID"),
            deviceId:this.state.device
        }

        getBaseline(body2)
            .then(
                response => {

                    this.setState({baseline:response})
                    console.log("BASELINE",this.state.baseline)

                }
            )
            .catch(error => {
                console.log(error)
            })

    }



    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };


    render(){

        return(

            <div>
                <UserMenu/>
                <TextField
                    style={{
                        marginTop:'30px',
                        marginLeft:'20px'
                    }}
                    name="device"
                    placeholder="Device Id"
                    onChange={this.handleInput}
                />
                <TextField
                    style={{
                        marginTop:'30px',
                        marginLeft:'20px'
                    }}
                    name="hours"
                    placeholder="Nr. of hours"
                    onChange={this.handleInput}
                />
                <Button
                    style={{
                        marginTop:'30px',
                        marginLeft:'20px'
                    }}
                    onClick={this.getProgram}

                    type = "submit"
                    variant="contained"
                    color="primary"
                >
                    Search
                </Button>



                <div>
                    <Bar
                        data={{
                            labels:['00:00',
                                '01:00',
                                '02:00',
                                '03:00',
                                '04:00',
                                '05:00',
                                '06:00',
                                '07:00',
                                '08:00',
                                '09:00',
                                '10:00',
                                '11:00',
                                '12:00',
                                '13:00',
                                '14:00',
                                '15:00',
                                '16:00',
                                '17:00',
                                '18:00',
                                '19:00',
                                '20:00',
                                '21:00',
                                '22:00',
                                '23:00',
                            ],
                            datasets: [
                                {
                                    label: 'Energy Consumption',
                                    data: this.state.graphValues,
                                    backgroundColor: [
                                        'rgba(103,253,93,0.3)'

                                    ],
                                    borderColor: [
                                        'rgb(255,255,255)'
                                    ],
                                    borderWidth: 1,
                                },
                                {
                                    label: 'Baseline',
                                    data: this.state.baseline,
                                    backgroundColor: [
                                        'rgba(0,111,255,0.3)'

                                    ],
                                    borderColor: [
                                        'rgb(182,182,182)'
                                    ],
                                    borderWidth: 1,
                                }
                            ],
                        }}
                        height={700}
                        width={700}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                            legend: {
                                labels: {
                                    fontSize: 25,
                                },
                            },
                        }}
                    />
                </div>
            </div>

        )

    }

}