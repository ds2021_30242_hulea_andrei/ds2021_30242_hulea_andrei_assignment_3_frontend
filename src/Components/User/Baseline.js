import React from 'react';
import UserMenu from "../Menu/UserMenu";
import {getBaseline} from "../../Rpc/rpcCall";
import {Button, TextField} from "@material-ui/core";
import {Bar} from "react-chartjs-2";

export default class Baseline extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            device:0,
            baseline:[]
        }

        this.getBaselineDetails = this.getBaselineDetails.bind(this)

    }

    getBaselineDetails(){

        let body = {
            userId:localStorage.getItem("USER_ID"),
            deviceId:this.state.device
        }

        getBaseline(body)
            .then(
                response => {
                    this.setState({baseline:response})
                    console.log(this.state.baseline)

                }
            )
            .catch(error => {
                console.log(error)
            })

    }

    handleInput = event => {

        const { value,name } = event.target;
        this.setState(
            {
                [name]: value
            }
        );
        console.log(value);
    };

    render(){
        return(
            <div >
                <UserMenu/>
                <TextField
                    style={{
                        marginTop:'100px',
                        marginLeft:'70px'
                    }}
                    name="device"
                    placeholder="Device Id"
                    onChange={this.handleInput}
                />
                <Button
                    style={{marginTop:'100px',marginLeft:'20px'}}

                    onClick={this.getBaselineDetails}
                    type = "submit"
                    variant="contained"
                    color="primary"
                >
                    Search
                </Button>



                <div>
                    <Bar
                        data={{
                            labels: ['00:00',
                                '01:00',
                                '02:00',
                                '03:00',
                                '04:00',
                                '05:00',
                                '06:00',
                                '07:00',
                                '08:00',
                                '09:00',
                                '10:00',
                                '11:00',
                                '12:00',
                                '13:00',
                                '14:00',
                                '15:00',
                                '16:00',
                                '17:00',
                                '18:00',
                                '19:00',
                                '20:00',
                                '21:00',
                                '22:00',
                                '23:00',
                            ],
                            datasets: [
                                {
                                    label: 'Energy Consumption',
                                    data: this.state.baseline,
                                    backgroundColor: [
                                        'rgba(0,111,255,0.3)'
                                    ],
                                    borderColor: [
                                        'rgb(255,255,255)'
                                    ],
                                    borderWidth: 1,
                                }
                            ],
                        }}
                        height={700}
                        width={700}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                            legend: {
                                labels: {
                                    fontSize: 25,
                                },
                            },
                        }}
                    />
                </div>
            </div>
        )
    }


}