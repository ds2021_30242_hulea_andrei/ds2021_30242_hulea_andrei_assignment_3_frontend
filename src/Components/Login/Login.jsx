import React, {useState} from "react";
import axiosInstance from "../../axios";
import {TextField, Button, Container} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import image from '../../Utils/Energy-Utility.jpg';

const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();

    const onChangeUsernameHandler = (e) => {
        setUsername(e.target.value)
    }

    const onChangePasswordHandler = (e) => {
        setPassword(e.target.value)
    }

    const onSubmitButton = () =>{

        let credentials = {
            username: username,
            password: password
        }

        axiosInstance.post("/authenticate", credentials)
            .then(
                res => {
                    console.log(res.data.jwtToken)
                    localStorage.setItem("TOKEN", res.data.jwtToken);


                    let token = res.data.jwtToken

                    let base64Url = token.split('.')[1]

                    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')

                    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
                        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                    }).join(''));

                    console.log(JSON.parse(jsonPayload))

                    let loginObj = JSON.parse(jsonPayload)

                    localStorage.setItem("USER", loginObj.USER);
                    localStorage.setItem("USER_ID", loginObj.USER_ID)

                    if (loginObj.USER === "Admin")
                        history.push("/admin/user")

                    if (loginObj.USER === "User")
                        history.push("/user")
                }
            )
            .catch(error => {
                console.log(error)
            })

        // axiosInstance.post("/login", credentials)
        //     .then(
        //         res => {
        //             localStorage.setItem("USER", res.data.role);
        //             localStorage.setItem("USER_ID", res.data.id)
        //
        //             if (res.data.role === "Admin")
        //                 history.push("/admin/user")
        //
        //             if (res.data.role === "User")
        //                 history.push("/user");
        //
        //         }
        //     )
        //     .catch(error => {
        //         console.log(error)
        //     })

    }




    return (

        <div style={{backgroundImage:'url(' + image + ')',height: '100%', position: 'absolute', left: '0px', width: '100%', overflow: 'hidden'}}>
        <Container maxWidth='100hv' >



            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '80vh'}}>

                <form >
                    <h1 style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        Log in
                    </h1>

                    <div>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Username"
                            placeholder="Username"
                            name="username"
                            autoComplete="string"
                            onChange={onChangeUsernameHandler}
                            autoFocus
                        />

                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            onChange={onChangePasswordHandler}
                            autoComplete="current-password"
                        />
                    </div>

                    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', 'marginTop': '10px'}}>
                        <Button
                            onClick={onSubmitButton}
                            variant="contained"
                            color="primary">
                            Sign In
                        </Button>
                    </div>

                </form>

            </div>

        </Container>
        </div>

    )


}
export default Login;







