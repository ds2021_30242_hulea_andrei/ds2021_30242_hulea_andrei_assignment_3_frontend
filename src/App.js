import './App.css';
import Login from './Components/Login/Login';
import AdminUsers from './Components/Admin/AdminUsers';
import AdminDevices from "./Components/Admin/AdminDevices";
import AdminSensors from "./Components/Admin/AdminSensors";
import UserDevices from './Components/User/UserDevices';
import UserHistory from "./Components/User/UserHistory";
import UserGraph from "./Components/User/UserGraph";
import DetailedGraph from "./Components/User/DetailedGraph"
import Baseline from "./Components/User/Baseline";
import Program from "./Components/User/Program"

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";


function App() {

  const defaultRoute = window.location.pathname === "/" ? <Redirect to = "/login"/> : undefined

  return (

      <Router>
        <Switch>
            <Route exact path="/login" component={Login}/>
            {localStorage.getItem("USER")==="Admin" && <Route exact path="/admin/user" component={AdminUsers}/>}
            {localStorage.getItem("USER")==="Admin" &&  <Route exact path="/admin/devices" component={AdminDevices}/>}
            {localStorage.getItem("USER")==="Admin" && <Route exact path="/admin/sensors" component={AdminSensors}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user" component={UserDevices}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user/history" component={UserHistory}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user/graph" component={UserGraph}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user/detailedgraph" component={DetailedGraph}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user/baseline" component={Baseline}/>}
            {localStorage.getItem("USER")==="User" &&<Route exact path="/user/program" component={Program}/>}

        </Switch>
        {defaultRoute}
      </Router>

  );
}

export default App;
